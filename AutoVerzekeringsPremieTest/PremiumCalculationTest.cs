﻿using System;
using Xunit;
using Moq;

using WindesheimAD2021AutoVerzekeringsPremie.Implementation;

namespace AutoVerzekeringsPremieTest
{
    public class PremiumCalculationTest
    {

        [Fact]
        public void PremiumIsCalculatedCorrectly()
        {
            // Arrange 
            Vehicle vehicle = new Vehicle(
                PowerInKw: 130,
                ValueInEuros: 500,
                constructionYear: 2005
                );

            // Act 
            double expectedPremiumcheck = 5;
            double actualPremiumcheck = PremiumCalculation.CalculateBasePremium(vehicle);
            // (Waarde voertuig / 100 - leeftijd + vermogen in KW / 5) / 3
            // (500 / 100 - 16 + 130 / 5) / 3


            // Assert
            Assert.Equal(expectedPremiumcheck, actualPremiumcheck);

        }

        [Theory]
        [InlineData(23, 4, true)]
        [InlineData(32, 4, true)]
        [InlineData(19, 1, true)]
        [InlineData(27, 3, true)]
        [InlineData(22, 5, true)]
        [InlineData(40, 10, false)]
        public void AgeUnder23AndLicenseAgeUnder5Has15PercentPremiumStrorage(int Age, int licenseAge, bool Expectation)
        {
            // Arrange
            int driverLicenseAge = DateTime.Now.Year - licenseAge;
            PolicyHolder policyHolder = new PolicyHolder(Age, $"01-01-{driverLicenseAge}", 8000, 0);

            // Act
            Vehicle vehicle = new Vehicle(110, 8000, 2002);
            PremiumCalculation premiumCalculation = new PremiumCalculation(vehicle, policyHolder, InsuranceCoverage.WA);

            double basePremium = PremiumCalculation.CalculateBasePremium(vehicle);
            

            if(Expectation)
            {
                basePremium *= 1.15;
            }

            // Assert
            Assert.Equal(basePremium, premiumCalculation.PremiumAmountPerYear);
        }

        [Theory]
        [InlineData(0, 21)]
        [InlineData(2, 21)]
        [InlineData(5, 21)]
        [InlineData(6, 19.95)]
        [InlineData(26, 7.35)]
        [InlineData(64, 7.35)]
        [InlineData(37, 7.35)]
        [InlineData(65, 7.35)]
        [InlineData(100, 7.35)]
        public void DiscountForNoClaimYears(int NoClaimYears, double ExpectedNoClaimPercentage)
        {
            // Arrange
            Vehicle vehicle = new Vehicle(100, 5000, DateTime.Now.Year - 5);
            PolicyHolder policyHolder = new PolicyHolder(25, $"05-09-{DateTime.Now.Year - 10}", 5000, NoClaimYears);

            // Act
            PremiumCalculation premiumCalculation = new PremiumCalculation(vehicle, policyHolder, InsuranceCoverage.WA);

            // Assert
            Assert.Equal(ExpectedNoClaimPercentage, premiumCalculation.PremiumAmountPerYear);
        }

        [Fact]
        public void DiscountForPremiumAmountPerYear()
        {
            // Arrange
            Vehicle vehicle = new Vehicle(200, 750, 2004);
            PolicyHolder policyHolder = new PolicyHolder(30, "09-04-2018", 4560, 4);
            Mock<PremiumCalculation> premiumCalculationMock = new Mock<PremiumCalculation>(vehicle, policyHolder, InsuranceCoverage.WA);
            premiumCalculationMock.SetupAllProperties().SetupGet(J => J.PremiumAmountPerYear).Returns(3000);

            // Act
            double expected = 2925;

            // Assert
            Assert.Equal(expected, premiumCalculationMock.Object.PremiumPaymentAmount(PremiumCalculation.PaymentPeriod.YEAR));

        }

        
    }
}
