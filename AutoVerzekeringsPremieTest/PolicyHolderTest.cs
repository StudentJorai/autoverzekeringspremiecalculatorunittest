using System;
using Xunit;

using WindesheimAD2021AutoVerzekeringsPremie.Implementation;

namespace AutoVerzekeringsPremieTest
{
    public class PolicyHolderTest
    {
        
        [Fact]
        public void ExceptionOnLicenseBeforeBirth()
        {
            // Arrange + Act + Assert
            Assert.Throws<ArgumentException>(() => new PolicyHolder(20, "01-01-1970", 1452, 2));
        }
        

        [Theory]
        [InlineData(25, "04-05-2017", 4)]
        [InlineData(12, "01-09-2013", 7)]
        [InlineData(40, "02-07-2010", 10)]
        [InlineData(20, "07-10-2045", -25)]
        public void DriverLicenseAgeIsCalculatedCorrectly(int Age, string DriverlicenseStartDate, int expectedAge)
        {
            // Arrange
            PolicyHolder policyHolder = new PolicyHolder(Age, DriverlicenseStartDate, 5783, 2);
            
            // Act + Assert
            Assert.Equal(expectedAge, policyHolder.LicenseAge);

        }


    }
}

