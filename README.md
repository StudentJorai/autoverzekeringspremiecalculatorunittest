# Beschrijving van UnitTesten door Jorai Jacobs

## PolicyHolder
In het bestand 'PolicyHolderTest' test ik verschillende requirements van de persoon die zijn premie wil berekenen en data in de applicatie invult. <br />
Allereerst bekijk ik of het programma accepteert dat je je rijbewijs kan halen voordat je geboren bent. Het resultaat van de testfunctie 'ExceptionOnLicenseBeforeBirth' laat zien dat het programma geen excepties of errors gooit als de gebruiker zijn behaalde rijbewijsdatum voor zijn geboortedatum invult. Dit had ik verwacht omdat ik al had uitgeprobeerd of je je rijbewijs kon behalen voordat je bent geboren. Het programma geeft hiervoor geen foutmelding en berekent gewoon de premie. Ik heb bij deze functie gebruikt gemaakt van een [Fact]. Daarom heb ik een if statement gemaakt die ervoor zorgt dat er wel een exceptie wordt gegooid en daardoor werkt de testfunctie wel.

In de testfunctie 'DriverLicenseAgeIsCalculatedCorrectly' bekijk ik of het programma goed berekent hoelang de persoon al zijn rijbewijs heeft. Ik heb hier gebruik gemaakt van een [Theory] en [InlineData]. Dit zijn de verschillende waardes die ik heb ingevuld voor de policyholder: <br />
*Leeftijd*: 25, 12 en 40 <br />
*Datum rijbewijs behaald*: 04-05-2017, 01-09-2013 en 02-07-2010 <br />
*Verwachte rijbewijs leeftijd* (hoelang heeft iemand zijn rijbewijs): 4, 7 en 10 <br />
De test is geslaagd en klopte met de waardes die zijn ingevuld. Het programma berekent dus hoelang iemand zijn rijbewijs heeft correct. Ik had dit verwacht omdat de code in het bestand 'PolicyHolder.cs' onder de functie 'AgeByDate' de leeftijd bereknt op een correcte manier.


## PremiumCalculation
In het bestand 'PremiumCalculationTest' test ik verschillende requirements voor de premie die wordt berekent door het programma. <br />
De functie 'PremiumIsCalculatedCorrectly' laat zien of de basispremie goed word uitgevoerd. Ik heb hier gebruik gemaakt van een [Fact]. De vehicle class heb ik in deze test de volgende waardes gegeven. <br />
*PowerInKW*: 130 <br />
*ValueInEuros*: 500 <br />
*constructionYear*: 2005 <br />
**Basispremie berekening**: (Waarde voertuig / 100 - leeftijd + vermogen in KW / 5) / 3 <br />
Als je de basispremie op deze manier berekent is het resultaat 5. Het programma daarentegen geeft een ander resultaat weer. Door deze test heb ik ontdekt dat er zat een fout in de code zat. Dit heb ik kunnen verbeteren en de test is geslaagd.

In het programma is er een requirement die zegt dat als je jonger bent dan 23 jaar of korter dan 5 jaar je rijbewijs hebt een premie-toeslag krijgt van 15%. In de testfunctie 'AgeUnder23AndLicenseAgeUnder5Has15PercentPremiumStrorage' test ik deze requirement. Ik heb hier gebruik gemaakt van een [Theory] en verschillende [InlineData]. Deze test is geslaagd en werkte prima naar verwachting.

In de 'DiscountForNoClaimYears' functie heb ik gekeken of het programma wel correct de korting berekent op basis van jouw schadevrije jaren. Ik heb hier gebruikt gemaakt van [InlineData] en dus ook een [Theory]. Deze functie gaf een fout wat ik niet verwachte. De resultaten kwamen niet overeen en dit kwam doordat een int, in het bestand 'PremiumCalculation.cs', veranderd moest worden naar een double. Dit voorkomt fractionloss. Toen ik dit had veranderd in de code slaagde de test.

In de functie 'DiscountForPremiumAmountPerYears' heb ik een Mock gebruikt. Ik heb hier ook gebruik gemaakt van een [Fact]. Het doel van deze test is om te zien of het programma de jaarbetaling met 2,5% korting goed kan berekenen. Ik had als verwachte resultaat 2925. Eerst is de test gefaald. Dit kwam doordat er een fout zat in de 'PremiumCalculation' onder 'PremiumPaymentAmount'. PremiumAmountPerYear was gedeeld door 1.025 gedaan. Dit moest * 0.975 zijn. Nadat ik dit heb aangepast slaagde de test.